package com.example.maprouteservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapRouteServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapRouteServiceApplication.class, args);
    }

}
