package com.example.maprouteservice.models;

import java.util.List;

public class Route {
    private String id;
    private Coordination start;
    private Coordination end;

    private List<Coordination> wps;

    public Route(String id, Coordination start, Coordination end, List<Coordination> wayPoints) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.wps = wayPoints;
    }

    public Coordination getStart() {
        return start;
    }

    public void setStart(Coordination start) {
        this.start = start;
    }

    public Coordination getEnd() {
        return end;
    }

    public void setEnd(Coordination end) {
        this.end = end;
    }


    public List<Coordination> getWps() {
        return this.wps;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
