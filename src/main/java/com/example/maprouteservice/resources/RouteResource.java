package com.example.maprouteservice.resources;

import com.example.maprouteservice.models.Coordination;
import com.example.maprouteservice.models.Route;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping("/")
public class RouteResource {

    private List<Route>  mockRoute(){
        List<Route> routeList = new ArrayList<>();



        Route route1 = new Route("route1",
                new Coordination(53.553780, 10.029130),
                new Coordination(53.588429, 9.994640),
        Arrays.asList(
                new Coordination(53.616339, 10.025883),
                new Coordination(53.607377, 10.113430),
                new Coordination(53.589370, 10.017860)
        ));
        Route route2 = new Route("route2",
                new Coordination(53.553780, 10.029130),
                new Coordination(53.588429, 9.994640),
                Arrays.asList(
                        new Coordination(53.616339, 10.025883),
                        new Coordination(53.607377, 10.113430),
                        new Coordination(53.589370, 10.017860),
                        new Coordination(53.592050, 10.044250),
                        new Coordination(53.551323, 9.965801),
                        new Coordination(53.540919, 10.004940),
                        new Coordination(53.557020,10.054890),
                        new Coordination(53.551731, 10.054035)
                ));
        Route route3 = new Route("route3",
                new Coordination(53.553780, 10.029130),
                new Coordination(53.588429, 9.994640),
                Arrays.asList(
                        new Coordination(53.616339, 10.025883),
                        new Coordination(53.607377, 10.113430),
                        new Coordination(53.589370, 10.017860)
                ));
        routeList.add(route1);
        routeList.add(route2);
        routeList.add(route3);
        return routeList;
    }



    @RequestMapping(value = "/routes")
    public String getAllRoutes(Model model)
    {
        model.addAttribute("routeList",mockRoute());
        return "jsonTemplate";
    }

    @RequestMapping(value = "/route")
    public String getAllRoutes(@RequestParam(value = "routeID") String routeID, Model model)
    {
        Route foundRoute = mockRoute().stream().filter( route -> routeID.equals(route.getId())).findAny().orElse(null);
        model.addAttribute(routeID,foundRoute);
        return "jsonTemplate";
    }


}
